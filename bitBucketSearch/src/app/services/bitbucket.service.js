"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var BitbucketService = (function () {
    function BitbucketService(_http) {
        this._http = _http;
        this.client_key = "uaGuXjANYNrGfnBRJe";
        this.client_secret = "376uSQmNE7cJgHe3nqYKUKym7tFUJYQ4";
        console.log("Bitbucket Service Ready");
        this.username = "pmendus";
    }
    BitbucketService.prototype.getUser = function () {
        return this._http.get("https://api.bitbucket.org/2.0/users/" + this.username + "?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(function (res) { return res.json(); });
    };
    BitbucketService.prototype.getRepositories = function () {
        return this._http.get("https://api.bitbucket.org/2.0/repositories/" + this.username + "?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(function (res) { return res.json(); });
    };
    BitbucketService.prototype.getFollowing = function () {
        return this._http.get("https://api.bitbucket.org/2.0/users/" + this.username + "/following?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(function (res) { return res.json(); });
    };
    BitbucketService.prototype.getFollowers = function () {
        return this._http.get("https://api.bitbucket.org/2.0/users/" + this.username + "/followers?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(function (res) { return res.json(); });
    };
    BitbucketService.prototype.updateUser = function (username) {
        this.username = username;
    };
    return BitbucketService;
}());
BitbucketService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], BitbucketService);
exports.BitbucketService = BitbucketService;
//# sourceMappingURL=bitbucket.service.js.map