import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class BitbucketService{
    private username:string;
    private client_key = "uaGuXjANYNrGfnBRJe";
    private client_secret = "376uSQmNE7cJgHe3nqYKUKym7tFUJYQ4";

    constructor(private _http: Http){
        console.log("Bitbucket Service Ready");
        this.username = "pmendus";
    }

    getUser(){
        return this._http.get("https://api.bitbucket.org/2.0/users/" + this.username +"?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(res => res.json());
    }
    getRepositories(){
        return this._http.get("https://api.bitbucket.org/2.0/repositories/" + this.username +"?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(res => res.json());
    }
    getFollowing(){
        return this._http.get("https://api.bitbucket.org/2.0/users/" + this.username +"/following?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(res => res.json());
    }
    getFollowers(){
        return this._http.get("https://api.bitbucket.org/2.0/users/" + this.username +"/followers?client_id=" + this.client_key + "&client_secret=" + this.client_secret)
            .map(res => res.json());
    }

    updateUser(username:string){
        this.username = username;
    }
}