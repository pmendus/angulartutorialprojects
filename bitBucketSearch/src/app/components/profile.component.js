"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var bitbucket_service_1 = require("../services/bitbucket.service");
var ProfileComponent = (function () {
    function ProfileComponent(_bitbucketService) {
        this._bitbucketService = _bitbucketService;
        this.user = false;
    }
    ProfileComponent.prototype.searchUser = function () {
        var _this = this;
        this._bitbucketService.updateUser(this.username);
        this._bitbucketService.getUser().subscribe(function (user) {
            _this.user = user;
        });
        this._bitbucketService.getRepositories().subscribe(function (repo) {
            _this.repo = repo;
            _this.repositories = repo.values;
        });
        this._bitbucketService.getFollowing().subscribe(function (following) {
            _this.following = following;
        });
        this._bitbucketService.getFollowers().subscribe(function (followers) {
            _this.followers = followers;
        });
    };
    return ProfileComponent;
}());
ProfileComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "profile",
        templateUrl: "profile.component.html",
    }),
    __metadata("design:paramtypes", [bitbucket_service_1.BitbucketService])
], ProfileComponent);
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map