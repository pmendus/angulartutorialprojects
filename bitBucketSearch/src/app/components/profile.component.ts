import { Component } from '@angular/core';
import {BitbucketService} from "../services/bitbucket.service";

@Component({
    moduleId: module.id,
    selector: "profile",
    templateUrl: "profile.component.html",
})
export class ProfileComponent  {
    username: string;
    user: any;
    repo: any[];
    repositories: any[];
    following: any[];
    followers: any[];

    constructor(private _bitbucketService: BitbucketService){
        this.user = false;
    }

    searchUser(){
        this._bitbucketService.updateUser(this.username);

        this._bitbucketService.getUser().subscribe(user => {
            this.user = user;
        });
        this._bitbucketService.getRepositories().subscribe(repo =>{
            this.repo = repo;
            this.repositories = repo.values;
        })
        this._bitbucketService.getFollowing().subscribe(following =>{
            this.following = following;
        })
        this._bitbucketService.getFollowers().subscribe(followers =>{
            this.followers = followers;
        })
    }
}