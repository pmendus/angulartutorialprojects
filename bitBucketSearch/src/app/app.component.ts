import { Component } from '@angular/core';
import {BitbucketService} from "./services/bitbucket.service";

@Component({
  selector: 'my-app',
  template: `
  <nav class="navbar navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">BitbucketSearch</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right"></form>
      </div><!--/.navbar-collapse -->
    </div>
  </nav>
  <div class="container">
    <profile></profile>
  </div>
  `,
  providers: [BitbucketService]
})
export class AppComponent  {}
